#encoding: utf-8
class BasePage < SitePrism::Page
  include RSpec::Matchers
  include Capybara::DSL
  require 'faker'


  element :nome_campo, '#fieldName'

  def click_togler (toggler)
    within_frame 0 do
      scroll_to(page.find(:xpath,'//div[label[span[.="'+toggler+'"]]]//div'),align: :top)
      page.find(:xpath,'//div[label[span[.="'+toggler+'"]]]//div').click
    end
  end

  def btn_next
    within_frame 0 do
      botao_proximo.click(wait: 50)
    end
  end
end