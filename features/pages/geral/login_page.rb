class LoginPage < SitePrism::Page

  element :user_name, '#cpLogin'
  element :password, '#cpPassword'

  def login_sistema(user,pass)
    visit_page
    self.user_name.set user
    self.password.set pass
    click_button('Entrar')
  end

  def visit_page (url = CONFIG['url'])
    visit url
  end
end