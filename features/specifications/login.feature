#language:pt
@logout @login_sis
Funcionalidade: Login

  Contexto:  Login
    Dado que estou na tela de login


  Cenario: Realizar login com usuário e senha válida
    Quando preencho com "rep771" e "1234"
    Entao devo ver o menu lateral do sistema

  Esquema do Cenario: Realizar login com usuário ou senha inválida
    Quando preencho com "<login>" e "<senha>"
    Entao devo ver "<mensagem>" em tela
    Exemplos:
      | login | senha| mensagem|
      | rep7712 | 1234| Representante não encontrado! |

