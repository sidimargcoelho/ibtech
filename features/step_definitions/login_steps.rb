  Dado(/^que estou na tela de login$/) do
    @Login.visit_page
  end

  Quando(/^preencho com "([^"]*)" e "([^"]*)"$/) do |usuario, senha|
    @usuario_logado = usuario
    find('#cpLogin').set @usuario_logado
    find('#cpPassword').set senha
    click_button('Entrar')
  end

  When(/^deve aprensentar em tela a "([^"]*)" de erro$/) do |mensagem|
    expect(page).to have_content mensagem
  end