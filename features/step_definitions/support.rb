# step de uso geral

Entao(/^devo ver "([^"]*)" em tela$/) do |mensagem|
  expect(page).to have_content mensagem
end

Entao(/^o menu lateral do sistema$/) do
  expect(page).to have_css 'nav'
end