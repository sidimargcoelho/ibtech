Before do
  Capybara.reset_sessions!
  Faker::Config.locale= 'pt-BR'
  @po = BasePage.new
  @Login = LoginPage.new
end

After('@logout')do
  begin
    click_button 'Sair'
     rescue StandardError => exception
    puts "Erro ao realizar Logout = #{exception}"
  end
end

After do |scenario|
  nomeCenario = scenario.name.gsub(/[^A-Za-z0-9]/ , '')
  nomeCenario =nomeCenario.gsub(' ', '_').downcase!
  screenshot = "log/screenshots/#{nomeCenario}.png"
  page.save_screenshot(screenshot)
  # embed(screenshot, 'image/png','Print')
end

Before('@login')do
  @Login.login_sistema CONFIG['loginadm'] , CONFIG['senhaadm']
end



